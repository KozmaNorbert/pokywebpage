<!doctype html>
<html lang="en">
  <head>
    <title>Poky | Galerija</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mainPage.css">

    <link rel="stylesheet" href="../css/fancybox.min.css">
    <link rel="stylesheet" href="../css/style.css">
    
</head>
  <body>

  <a href="../index.php">
<img alt="hun" src="../images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">  
<img alt="srb" src="../images/srb.png" width="30" height="20">
</a>
      <div class="top-container">
        <div><img src="../images/1.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/2.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/3.jpg" width="100%"  alt="" class="top-container"></div> 
      </div>
    <!--Navbar-->
   
      <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
        <img src="../images/logo.png" width="4%"  alt="" >
          <div class="nav navbar-nav">
              
              <a class="nav-item nav-link" href="index.php">Glavna stranica</a> 
              <a class="nav-item nav-link" href="about.php">O nama</a>
              <a class="nav-item nav-link" href="history.php">O kolaču</a>
              <a class="nav-item nav-link" href="supply.php">Snabdevanje</a>
              <a class="nav-item nav-link" href="events.php">Događaji</a>
              <a class="nav-link active" href="gallery.php">Galerija</a>
              <a class="nav-item nav-link" href="search.php">Pretraga</a>
          </div>
      </nav>    
<br>
<br>
<br>

      <?php
      
      define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
      require("../admin/db_config.php");
$pathForPictures = '/pokywebpage/picsGallery/';


  $querySelectChimneyCake = "SELECT *
                            FROM gallery";

  $result = mysqli_query($connection,$querySelectChimneyCake) or die(mysqli_error($connection));

  if(mysqli_num_rows($result)>0)
  {
    echo "<main class='main-content'> 
            <div class='container-fluid photos'>
            <div class='row align-items-stretch'>";
    while ($record=mysqli_fetch_array($result,MYSQLI_ASSOC))
    {
        $isAvailable = 'nem';
        if($record['IMAGE_ISAVAILABLE'] == 1){
            $isAvailable = 'igen';
        }

      

      echo  "<div class='col-6 col-md-6 col-lg-4'>";
      echo  "<a href=".$pathForPictures.''.$record['IMAGE_FILE_NAME'];
      echo  " class='d-block photo-item'";
      echo  "data-fancybox='gallery'>";
      echo  "<img src=".$pathForPictures.''.$record['IMAGE_FILE_NAME'];
      echo  " alt='Image' class='img-fluid'>";
      echo  "<div class='photo-text-more'>";
      echo  "<span class='icon icon-search'></span>";
      echo   "</div>
                </a>
              </div>";

     
    }
          echo    "</div>
          </div>
          </div>
          </div>
          </main>";
  }
  ?>


   
      <script src="js/jquery-3.3.1.min.js"></script>
      <script src="js/jquery-migrate-3.0.1.min.js"></script>
      <script src="js/jquery-ui.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.stellar.min.js"></script>
      <script src="js/jquery.countdown.min.js"></script>
      <script src="js/jquery.magnific-popup.min.js"></script>
      <script src="js/bootstrap-datepicker.min.js"></script>
      <script src="js/aos.js"></script>
    
      <script src="js/jquery.fancybox.min.js"></script>
    
      <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
    <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Prijavi se</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail adresa">
          <button type="button" class="btn btn-light">Prihvati</button><br>
          <p><a href="index.php">Dodatne informacije</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="../images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="../images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>