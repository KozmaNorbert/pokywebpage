<!doctype html>
<html lang="en">

<head>
  <title>Poky | Kürtöskalácsról</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/bootstrap.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/mainPage.css">

</head>
</head>

<body>
  <?php
  define("SECRET", "@3eweHjdsdfuihjhjhj#VGVgggg!");
  require("../admin/db_config.php");
  ?>
      <a href="../index.php">
<img alt="hun" src="../images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">  
<img alt="srb" src="../images/srb.png" width="30" height="20">
</a>
      <div class="top-container">
        <div><img src="../images/1.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/2.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/3.jpg" width="100%"  alt="" class="top-container"></div> 
      </div>
    <!--Navbar-->
   
      <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
        <img src="../images/logo.png" width="4%"  alt="" >
          <div class="nav navbar-nav">
              
              <a class="nav-item nav-link" href="index.php">Glavna stranica</a> 
              <a class="nav-item nav-link" href="about.php">O nama</a>
              <a class="nav-link active" href="history.php">O kolaču</a>
              <a class="nav-item nav-link" href="supply.php">Snabdevanje</a>
              <a class="nav-item nav-link" href="events.php">Događaji</a>
              <a class="nav-item nav-link" href="gallery.php">Galerija</a>
              <a class="nav-item nav-link" href="search.php">Pretraga</a>
    </div>
  </nav>
  <br>
  

  <div class="test-content">
    <div class="test text">
      <h2>Priča o torti za dimnjake</h2><br>
      
      Još se dosta raspravlja o dimnjačkim tortama, bilo da su to ustvari dimnjačarski kolači ili pecivo za dimnjake ako su
       gore spomenuta torta.
       Neki veruju da je ime torta od roga pogodnije jer se ova torta može pratiti do vremena osvajanja,
       da su Mađari koji su upadali, ako su pljačkali brašno i jaja, izmetali testo od njega, a zatim ga ispekli tako što su ga smotali na rogove
       kod vatre. Drugi smatraju da dimnjačka torta nema nikakve veze sa osvajačima Mađara, jer je ova torta bila samo
        napravljene su kada su se već naselili i imali stabilnu kuhinju.</br></br>
       <img src="../allPictures/1.jpg" alt="Image" class="img-fluid text test-content"></br></br>

       Prema drugoj teoriji, dimnjak je dobio ime po peći, pošto se u Seklerlandu peć na cevi naziva dimnjak,
        veličina i prečnik dimnjačkog kolača takođe ukazuju na to. Genijalni Szekler je žito od tvrdog drva koje on kuva i
        korišćen za grejanje, i dalje je želeo da ga iskoristi za nešto, pa su užarene žuljeve odnele na vrh peći ili u rernu
        ispred nje i tamo su pekli ovu deliciju prevrćući je nad žernjavom.</br></br>

        Postoji više verovanja i izreka o poreklu dimnjaka. Naravno, izjavljujemo sebi to
         je najbliži. Ovo se odnosi na tatarski okrug: "Pristup tatarskim vojskama nakon prvobitnog otpora, stanovništvu Szeklerlanda
         ugledao je dobro da bježi u planine, neki su utočište pronašli u planinama, dok su drugi utočište pronašli u pećinama Budvar i Rez. THE
         a Tatari ih nisu mogli napasti ili namamiti u sigurno i nepristupačno skrovište,
          oni su odlučili da gladuju Seklere. To je trajalo dugo, do tada samo za Tatare i Šeklere
          ponestalo im je hrane. Tada je pametna žena Szekler isekla preostalo brašno, pomešala ga sa pepelom i pojela ogromnu količinu
          pekli su kolače koji su crtani na doroncima ili visokim motkama i predočeni Tatarima: „Pogledajte kako dobro ovde
          živimo dok gladujete. "Tatari, koji su jedva podnosili glad, povukli su se iznervirani."
          </br></br>
          <img src="../allPictures/430.jpg" alt="Image" class="img-fluid text test-content"></br></br>

      <br><br>
    </div>
  </div>
  <br>
  <br>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Prijavi se</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail adresa">
          <button type="button" class="btn btn-light">Prihvati</button><br>
          <p><a href="index.php">Dodatne informacije</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="../images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="../images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>