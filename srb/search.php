<!doctype html>
<html lang="en">

<head>
  <title>Poky | Pretraga</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/bootstrap.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/mainPage.css">

</head>

<body>
<a href="../index.php">
<img alt="hun" src="../images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">  
<img alt="srb" src="../images/srb.png" width="30" height="20">
</a>
      <div class="top-container">
        <div><img src="../images/1.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/2.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/3.jpg" width="100%"  alt="" class="top-container"></div> 
      </div>
    <!--Navbar-->
   
      <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
        <img src="../images/logo.png" width="4%"  alt="" >
          <div class="nav navbar-nav">
              
              <a class="nav-item nav-link" href="index.php">Glavna stranica</a> 
              <a class="nav-item nav-link" href="about.php">O nama</a>
              <a class="nav-item nav-link" href="history.php">O kolaču</a>
              <a class="nav-item nav-link" href="supply.php">Snabdevanje</a>
              <a class="nav-item nav-link" href="events.php">Događaji</a>
              <a class="nav-item nav-link" href="gallery.php">Galerija</a>
              <a class="nav-link active" href="search.php">Pretraga</a>
      
    </div>
  </nav>
  <br>

  <div class="container">
   <br />
   <h2>Pretražite događaje</h2><br>
   <div class="form-group">
    <div class="input-group">
     <span class="input-group-addon">Pretraga</span>
     <input type="text" name="search_text" id="search_text"  class="form-control" />
    </div>
   </div>
   <br />
   <div id="result"></div>
  </div>
  <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
  <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Prijavi se</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail adresa">
          <button type="button" class="btn btn-light">Prihvati</button><br>
          <p><a href="index.php">Dodatne informacije</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="../images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="../images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>

<script>
$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:"fetch.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>