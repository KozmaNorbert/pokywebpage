<!doctype html>
<html lang="en">

<head>
  <title>Poky | O nama</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/bootstrap.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/mainPage.css">

</head>
</head>

<body>
  <?php
  define("SECRET", "@3eweHjdsdfuihjhjhj#VGVgggg!");
  require("../admin/db_config.php");
  ?>
 <a href="../index.php">
<img alt="hun" src="../images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">  
<img alt="srb" src="../images/srb.png" width="30" height="20">
</a>
      <div class="top-container">
        <div><img src="../images/1.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/2.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/3.jpg" width="100%"  alt="" class="top-container"></div> 
      </div>
    <!--Navbar-->
   
      <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
        <img src="../images/logo.png" width="4%"  alt="" >
          <div class="nav navbar-nav">
              
              <a class="nav-item nav-link" href="index.php">Glavna stranica</a> 
              <a class="nav-link active" href="about.php">O nama</a>
              <a class="nav-item nav-link" href="history.php">O kolaču</a>
              <a class="nav-item nav-link" href="supply.php">Snabdevanje</a>
              <a class="nav-item nav-link" href="events.php">Događaji</a>
              <a class="nav-item nav-link" href="gallery.php">Galerija</a>
              <a class="nav-item nav-link" href="search.php">Pretraga</a>
    </div>
  </nav>
  <br>
  

  <div class="test-content">
    <div class="test text">
      <h2>Kratko o našem porodičnom preduzeću</h2><br>
      
      Naša porodica je 2007 godine je počela da se bavi izradom i prodajom Kirteškolača u Subotici i u  okolnima selima. Glava porodice 
      je izuzetan bravar koji je sam izumeo našu mašinu, svojim rukama je sastavio šator za prodaju i sve prateće stolove i neophodne 
      alate za ovaj zanat. Prepoznali smo potencijal u našoj ideji da se pokrene biznis iako smo bili jedan od prvih koji su počeli da se 
      bave ovim zanatom. Redovno smo posećivali vašare i razne dogadjaje, manifestacije i pijace. Ljudima je bilo jako interesantno šta 
      je to što se vrti na žaru. Prvih nekoliko meseci smo morali da pričamo o kolaču, kojeg još nikada nisu videli ranije, da pričamo o 
      njegovoj tradiciji i poreklom. Danas skoro nema naselja, gde nisu čuli za ovaj kolač.
  Polako smo izgradili naše tržište, dobijali smo pozive na sve strane. Sedište firme je u Subotici, ali se pojavljujemo na celoj 
  teritoriji države na manifestacijima i sličnim dogadjajima. Na početku smo nudili kolače u šest ukusa a to su: cimet, lešnik, 
  čokolada, orasi, vanila i kokos. Kasnije smo dodali još neke savremenije ukuse kao što su Nutella i keks, Nutella i lešnik, Nutella 
  i kokos, Žerbo i slično, koje su jako popularne.
Svakog meseca mogu da nas nadju naši cenjeni kupci: prve nedelje je vašar u Senti, druga nedelja Bačka Topola, treća nedelja je 
Čantavir. Preko leta posećujemo koncerte, manifestacije i slično, a preko zime smo svake godine u centru grada Subotice za vreme
 manifestacije 
<br>
      <br>
      Tok posla<br>
      <br>
      Rano ujutru krećemo na vašare. Kad stignemo, postavljamo naš štand, šator ili drvenu kućicu u zavisnosti tipa dogadjaja. Kad se 
      postavimo, namesimo testo po našem receptu koji smo formirali niz godina da bi je usavršili. Namotamo testo na drveni kalup, pa 
      uvaljamo u šećer i stavimo kolač u mašinu gde ćemo da ga ispečemo na žaru. Baš zbog ovoga neki i zovu kolač na žaru. Kada se šećer 
      karamelizuje uvaljamo u neki ukus, koje smo pomenuli ranije, ukus se zalepi za vrelom karamelom. Nakon toga skinemo sa kalupa i 
      spakujemo ga u naše vrećice koje su označeni znakom naše firme. Kada se manifestacija završi, rastavljamo šator, ugasimo i uredno
       odložimo žar.

      <br><br>
    </div>
  </div>
  <br>
  <br>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Prijavi se</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail adresa">
          <button type="button" class="btn btn-light">Prihvati</button><br>
          <p><a href="index.php">Dodatne informacije</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="../images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="../images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>