<!doctype html>
<html lang="en">
  <head>
    <title>Poky | Glavna stranica</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.css">
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/mainPage.css">
    
  </head>
</head>
  <body>
    <?php
        define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
        require("../admin/db_config.php");
    ?>
       <a href="../index.php">
<img alt="hun" src="../images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">  
<img alt="srb" src="../images/srb.png" width="30" height="20">
</a>
      <div class="top-container">
        <div><img src="../images/1.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/2.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="../images/3.jpg" width="100%"  alt="" class="top-container"></div> 
      </div>
    <!--Navbar-->
   
      <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
        <img src="../images/logo.png" width="4%"  alt="" >
          <div class="nav navbar-nav">
              
              <a class="nav-link active" href="index.php">Glavna stranica</a> 
              <a class="nav-item nav-link" href="about.php">O nama</a>
              <a class="nav-item nav-link" href="history.php">O kolaču</a>
              <a class="nav-item nav-link" href="supply.php">Snabdevanje</a>
              <a class="nav-item nav-link" href="events.php">Događaji</a>
              <a class="nav-item nav-link" href="gallery.php">Galerija</a>
              <a class="nav-item nav-link" href="search.php">Pretraga</a>
          </div>
      </nav>    
      <br>
      <br>
      <br>
      <div class="test-content">
        <div class="test text" >
        Poštovani Inkuirer!<br>
        <br>
        „POKI Kolacs na žaru“ je našim dragim kupcima na raspolaganju već 11 godina. Nastupili smo na mnogim gastronomskim izložbama, sajmovima, raznim manifestacijama, kao što su prvomajski dan, takođe dani pršuta na Paliću, ceremonije žetve, Dan Svetog Stefana u Magiarkanizsi i Paliću, Festival hleba u Novom Sadu itd.<br>
        Specijalnost:<br>
        - sveži proizvodi dok testo mesimo ručno i lokalno,<br>
        - izbor različitih varijacija ukusa,<br>
        - kupac može da sledi sve korake za pravljenje dimnjaka,<br>
        - naši zaposleni nose jedinstveno dizajniranu radnu odeću sa logotipom „POKY“.<br>
      <br>
     
      Izrada tradicionalnih, domaćih dimnjačkih kolača, na tradicionalnim sajmovima, festivalima, demonstracijama zanata, gastronomskim izložbama, raznim okupljanjima, balovima, sajmovima ili bilo kojim drugim organizovanim događajima.<br>
      
      <br>
      Dpagi potrošači,

      S obzirom na to da u bliskoj budućnosti neće biti događaja, odlučili smo da odnesemo tortu za dimnjake kući.


    </div>
    <div class="main-page-image" >
        <img src="../allPictures/cimetke.jpg" alt="Image" class="img-fluid text">
        </div>
    </div>
      </div>
     

    <section class="bg-info text-center p-5 mt-4">
      <div class="container p-3">
              <br>
              <br>
              <h3>Poštovani posetioci!</h3><br>
  <p>Ako ne želite propustiti događaje kojima takođe prisustvujemo, prijavite se na našu stranicu! Sve što trebate učiniti je unijeti svoju adresu e-pošte i mi ćemo vas obavestiti o nadolazećim događajima!<p>
        
       
        <form action="" method="post">
          <input type="text" name="email_address" placeholder=" E-mail adresu  ">
          <input type="submit" name="submit" value="Prijavi se" class="btn btn-light">
        </form>

        <?php
        if(isset($_POST['submit'])){ 
        
          $email = $_POST["email_address"];
        $emailErr = "Netačan obrazac za e-mailu";
        $emailSuccess = "Pretplata je prihvaćena";
        $emailInsertError = "Nešto nije u redu. Molim vas, pokušajte ponovo!";
        $emailIsExist = "Već ste pretplaćeni na ovaj email";

        $querySelectEmail = "SELECT * FROM subscribers WHERE EMAIL = '$email'";
        $result = mysqli_query($connection,$querySelectEmail) or die(mysqli_error($connection));
        if(mysqli_num_rows($result) == 0){
         
      

          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo "<script type='text/javascript'>alert('$emailErr');</script>";
          }
          else{

              $queryUploadPicture = "INSERT INTO subscribers (EMAIL) VALUES ('$email')"; 
              $insert = $connection->query($queryUploadPicture);
                
                if ($insert) {
                  echo "<script type='text/javascript'>alert('$emailSuccess');</script>";
                }
                else {
                  echo "<script type='text/javascript'>alert('$emailInsertError');</script>";
                }
        }
        }
     
      elseif(mysqli_num_rows($result) != 0){
        echo "<script type='text/javascript'>alert('$emailIsExist');</script>";
      }
    }
        ?>
        
        <br><br>
      </div>
    </section>
   <br>
   <br>
     
     
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
    <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Prijavi se</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail adresa">
          <button type="button" class="btn btn-light">Prihvati</button><br>
          <p><a href="index.php">Dodatne informacije</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="../images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="../images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>