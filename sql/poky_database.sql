-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2020 at 10:01 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poky_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idADMIN` int(11) NOT NULL,
  `USERNAME_ADMIN` varchar(45) DEFAULT NULL,
  `EMAIL_ADMIN` varchar(45) NOT NULL,
  `PASSWORD_ADMIN` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idADMIN`, `USERNAME_ADMIN`, `EMAIL_ADMIN`, `PASSWORD_ADMIN`) VALUES
(1, 'Test Admin', 'test@poky.com', '$2y$10$rzAtrkjrBCf5YnIKymWB5OTRWWgWr4mPLMHP/LkQiAFCMWBGrCmGa');

-- --------------------------------------------------------

--
-- Table structure for table `chimney_cakes`
--

CREATE TABLE `chimney_cakes` (
  `id` int(11) NOT NULL,
  `CHIMNEYCAKE_NAME` varchar(45) NOT NULL,
  `CHIMNEYCAKE_KAL` varchar(45) DEFAULT NULL,
  `CHIMNEYCAKE_FLAVOR` varchar(45) NOT NULL,
  `CHIMNEYCAKE_DESC` varchar(45) NOT NULL,
  `CHIMNEYCAKE_LANG` varchar(2) NOT NULL,
  `CHIMNEYCAKE_ISAVAILABLE` tinyint(1) DEFAULT 1,
  `CHIMNEYCAKE_IMAGES_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chimney_cakes`
--

INSERT INTO `chimney_cakes` (`id`, `CHIMNEYCAKE_NAME`, `CHIMNEYCAKE_KAL`, `CHIMNEYCAKE_FLAVOR`, `CHIMNEYCAKE_DESC`, `CHIMNEYCAKE_LANG`, `CHIMNEYCAKE_ISAVAILABLE`, `CHIMNEYCAKE_IMAGES_ID`) VALUES
(53, 'Fahéj', '300', '', '', 'hu', 1, 35),
(54, 'Cimet', '300', '', '', 'sr', 1, 35);

-- --------------------------------------------------------

--
-- Table structure for table `chimney_cake_images`
--

CREATE TABLE `chimney_cake_images` (
  `id` int(11) NOT NULL,
  `IMAGE_FILE_NAME` varchar(45) NOT NULL,
  `IMAGE_ALT` varchar(45) DEFAULT NULL,
  `IMAGE_ISAVAILABLE` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chimney_cake_images`
--

INSERT INTO `chimney_cake_images` (`id`, `IMAGE_FILE_NAME`, `IMAGE_ALT`, `IMAGE_ISAVAILABLE`) VALUES
(35, '1590258741_5ec96c355ee5c.jpg', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name_hun` varchar(50) NOT NULL,
  `name_srb` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `place_hun` varchar(45) NOT NULL,
  `place_srb` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name_hun`, `name_srb`, `start_date`, `end_date`, `place_hun`, `place_srb`) VALUES
(9, 'Téli vásár', 'Zimski vasar', '2020-12-15', '2021-01-05', 'Szabadka', 'Subotica');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `IMAGE_FILE_NAME` varchar(45) NOT NULL,
  `IMAGE_DESC` varchar(45) DEFAULT NULL,
  `IMAGE_ALT` varchar(45) DEFAULT NULL,
  `IMAGE_ISAVAILABLE` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `IMAGE_FILE_NAME`, `IMAGE_DESC`, `IMAGE_ALT`, `IMAGE_ISAVAILABLE`) VALUES
(9, '1588183766_5ea9c2d6dce8e.jpg', '', '', 1),
(11, '1588183883_5ea9c34bcffcf.jpeg', 'gallery/1588183883_5ea9c34bcffcf.jpeg', '1588183883_5ea9c34bcffcf.jpeg', 1),
(12, '1588186849_5ea9cee1ec7dc.jpg', 'gallery/1588186849_5ea9cee1ec7dc.jpg', '1588186849_5ea9cee1ec7dc.jpg', 1),
(13, '1588335262_5eac129e085f3.jpg', 'gallery/1588335262_5eac129e085f3.jpg', '1588335262_5eac129e085f3.jpg', 1),
(14, '1588359357_5eac70bd4d7bb.jpg', 'gallery/1588359357_5eac70bd4d7bb.jpg', '1588359357_5eac70bd4d7bb.jpg', 1),
(15, '1588359559_5eac7187c164b.jpg', NULL, NULL, 1),
(16, '1588359944_5eac73086c204.jpg', NULL, NULL, 1),
(23, '1593891092_5f00d9143e23b.jpg', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `EMAIL` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `EMAIL`) VALUES
(7793, 'gd96@gmail.com'),
(7794, 'k.norbi@gmail.com'),
(7795, 'asd@asd.asd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idADMIN`);

--
-- Indexes for table `chimney_cakes`
--
ALTER TABLE `chimney_cakes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CHIMNEYCAKE_IMAGES_ID` (`CHIMNEYCAKE_IMAGES_ID`);

--
-- Indexes for table `chimney_cake_images`
--
ALTER TABLE `chimney_cake_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `idADMIN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chimney_cakes`
--
ALTER TABLE `chimney_cakes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `chimney_cake_images`
--
ALTER TABLE `chimney_cake_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7796;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chimney_cakes`
--
ALTER TABLE `chimney_cakes`
  ADD CONSTRAINT `chimney_cakes_ibfk_1` FOREIGN KEY (`CHIMNEYCAKE_IMAGES_ID`) REFERENCES `chimney_cake_images` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
