<h1>Események szerkeztése</h1><br>
<h2>Új Esemény</h2>

<form method="post" action="" enctype='multipart/form-data'>
   <label for="name">Rendezvény neve:</label><br>
   <input type="text" id="name" name="name" maxlength="45"><br>

  <label for="name">Rendezvény neve szerbül:</label><br>
  <input type="text" id="name-sr" name="name-sr" maxlength="45"><br><br>

  <label for="fname">Rendezvény helyszine:</label><br>
  <input type="text" id="city" name="city" maxlength="45"><br><br>

  <label for="fname">Rendezvény helyszine szerbül:</label><br>
  <input type="text" id="city-sr" name="city-sr" maxlength="45"><br><br>

  <label for="fname">Esemény kezdete:</label><br>
    <input type="date" id="startDate" name="startDate"><br><br>

    <label for="fname">Esemény vége:</label><br>
    <input type="date" id="endDate" name="endDate"><br><br>
    <input type='submit' value='Esemény mentése' name='submit'>
</form><br>

<?php 

if(isset($_POST['submit'])){ 
  
    $eventsNameHu = $_POST['name'];
    $eventsNameSr = $_POST['name-sr'];

    $eventsCityHu = $_POST['city'];
    $eventsCitySr = $_POST['city-sr'];

    $eventsStart = $_POST['startDate'];
    $eventsEnd = $_POST['endDate'];

    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = ''; 
    if($eventsNameHu == '' || $eventsNameSr == '' || $eventsCityHu == '' || $eventsCitySr == '' || $eventsStart == '' || $eventsEnd == '' ){
      $statusMsg = "Töltsön ki minden mezőt.".$errorMsg;
    }

   
    $queryEventINSERT ="INSERT INTO events (
        name_hun,
        name_srb,
        start_date,
        end_date,
        place_hun,
        place_srb)
        Values(
          '$eventsNameHu',
          '$eventsNameSr',
          '$eventsStart',
          '$eventsEnd',
          '$eventsCityHu',
          '$eventsCitySr'
         
        )";
    if( $statusMsg == ''){
        $insertEventStatus = $connection->query($queryEventINSERT);
                if($insertEventStatus){
                    $statusMsg = "Esemény sikeresen hozzáadva! ".$errorMsg;
                }
            else{
                $statusMsg = "Valami hiba lépett fel a hozzáadás során..."; 
            } 
          }
            echo "<script type='text/javascript'>alert('$statusMsg');</script>";
          }
?>

<div id="content">
<hr>
  <h1>Esemény lista</h1>
<?php

  $querySelectEvents = "SELECT id,
                              name_hun,
                              name_srb,
                              start_date,
                              end_date,
                              place_hun,
                              place_srb
                              from events
                            ";

  $result = mysqli_query($connection,$querySelectEvents) or die(mysqli_error($connection));
  if(mysqli_num_rows($result) == 0){
    echo '<p>Jelenleg nincs elérhető esemény!</p>';
  }

  if(mysqli_num_rows($result)>0)
  {
   
    $no=1;
echo '<style>
table, th, td {
  border: 1px solid black;
}
</style>';

    echo '<table ';
    echo '<tr>
            <th>No.</th>
            <th>Esemény neve hun</th>
            <th>Esemény neve srb</th>
            
            <th>Esemény kezdete</th>
            <th>Esemény vége</th>
            <th>Esemény helyszine hun</th>
            <th>Esemény helyszine srb</th>
           
            <th>Műveletek</th>    
        </tr>';
    
    
    while ($record=mysqli_fetch_array($result,MYSQLI_ASSOC))
    {

      echo "<tr>";

      echo '<td>'.$no.'.</td>
            <td>'.$record['name_hun'].'</td>
            <td>'.$record['name_srb'].'</td>
            <td>'.$record['start_date'].'</td>
            <td>'.$record['end_date'].'</td>
            <td>'.$record['place_srb'].'</td>
            <td>'.$record['place_hun'].'</td>
            </br>';
          
      echo '<td>
           <a href="deleter.php?page=events&amp;id='.$record['id'].'">törlés</a> 
           
           </td>';
      echo "</tr>";
      $no++;
    }
    echo '</table>';
    mysqli_free_result($result);
  }