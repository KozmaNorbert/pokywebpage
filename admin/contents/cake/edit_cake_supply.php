<h1>Kalácskínálat szerkeztése</h1><br>
<h2>Új kalács hozzáadása</h2>

<form method="post" action="" enctype='multipart/form-data'>
  <label for="name">Kalács elnevezése:</label><br>
  <input type="text" id="name" name="name" maxlength="45"><br>
  <label for="name">Kalács elnevezése szerbül:</label><br>
  <input type="text" id="name-sr" name="name-sr" maxlength="45"><br><br>

  <label for="fname">Kalács ízesítése:</label><br>
  <input type="text" id="flavor" name="flavor" maxlength="45"><br>
  <label for="fname">Kalács ízesítése szerbül:</label><br>
  <input type="text" id="flavor-sr" name="flavor-sr" maxlength="45"><br><br>

  <label for="fname">Kalács leírása:</label><br>
  <input type="text" id="desc" name="desc" maxlength="45"><br>
  <label for="fname">Kalács leírása szerbül:</label><br>
  <input type="text" id="desc-sr" name="desc-sr" maxlength="45"><br><br>

  <label for="lname">Kalória:</label><br>
  <input type="text" id="cal" name="cal" maxlength="45"><br><br>

    <input type='file' name='file' accept="image/*"><br>
    <input type='submit' value='Kalács mentése' name='submit'>
</form><br>

<?php 
//define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
//require("db_config.php");
if(isset($_POST['submit'])){ 
    // Include the database configuration file 
    //include_once '../../db_config.php'; 
     
    // File upload configuration 
    $targetDir = "../../../picsChimneycake/"; 
    $allowTypes = array('jpg','png','jpeg','gif');

    $chimneyNameHu = $_POST['name'];
    $chimneyNameSr = $_POST['name-sr'];

    $chimneyFlavorHu = $_POST['flavor'];
    $chimneyFlavorSr = $_POST['flavor-sr'];

    $chimneyDescHu = $_POST['desc'];
    $chimneyDescSr = $_POST['desc-sr'];

    $chimneyCal = $_POST['cal'];
    
//     CHIMNEYCAKE_NAME
//     CHIMNEYCAKE_KAL
//     CHIMNEYCAKE_FLAVOR
//     CHIMNEYCAKE_DESC
//     CHIMNEYCAKE_LANG
//     CHIMNEYCAKE_ISAVAILABLE


    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = ''; 
    $fileNames = $_FILES['file']['name']; 
    if(!empty($fileNames)){ 
        //$$_FILES['file']['name'] as $key=>$val
            // File upload path 
            $fileName = $_FILES['file']['name'];
            echo $fileName;
            $targetFilePath = $targetDir . $fileName; 
             
            // Check whether file type is valid 
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
            //echo $fileType;
            if(in_array($fileType, $allowTypes)){ 
                // Upload file to server
                $newFilename = time() . "_" . uniqid() . "." . $fileType;
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetDir . $newFilename)){ 
                    // Image db insert sql 
                    //$insertValuesSQL .= "('".$newFilename."')";
                    $queryUploadPicture = "INSERT INTO chimney_cake_images (IMAGE_FILE_NAME) VALUES ('$newFilename')"; 
                }else{ 
                    $errorUpload .= $_FILES['file']['name']; 
                } 
            }else{ 
                $errorUploadType .= $_FILES['file']['name']; 
            } 
        
         
        if(!empty($queryUploadPicture)){ 
            $insertValuesSQL = trim($insertValuesSQL, ',');
            echo $insertValuesSQL;
            // Insert image file name into database 
            $insert = $connection->query($queryUploadPicture); 
            if($insert){ 
                $errorUpload = !empty($errorUpload)?'Upload Error: '.trim($errorUpload, ' | '):''; 
                $errorUploadType = !empty($errorUploadType)?'File Type Error: '.trim($errorUploadType, ' | '):''; 
                $errorMsg = !empty($errorUpload)?'<br/>'.$errorUpload.'<br/>'.$errorUploadType:'<br/>'.$errorUploadType; 
                //$insertedPictureId = $connection->query("INSERT INTO chimney_cakes () values"); //TODO
                
                        //INSERT CHIMNEYCACE QUERY - START
                        $queryChimneyCaceINSERT ="INSERT INTO chimney_cakes (
                            CHIMNEYCAKE_NAME, 
                            CHIMNEYCAKE_KAL, 
                            CHIMNEYCAKE_FLAVOR, 
                            CHIMNEYCAKE_DESC,
                            CHIMNEYCAKE_LANG,
                            CHIMNEYCAKE_ISAVAILABLE,
                            CHIMNEYCAKE_IMAGES_ID) 
                            VALUES( 
                                '$chimneyNameHu', 
                                '$chimneyCal', 
                                '$chimneyFlavorHu', 
                                '$chimneyDescHu',
                                'hu', 
                                '1', 
                                (SELECT id FROM chimney_cake_images WHERE IMAGE_FILE_NAME = '$newFilename')),
                            (
                            '$chimneyNameSr',
                            '$chimneyCal',
                            '$chimneyFlavorSr',
                            '$chimneyDescSr',
                            'sr',
                            '1',
                                (SELECT id FROM chimney_cake_images WHERE IMAGE_FILE_NAME = '$newFilename'))";
                        //INSERT CHIMNEYCACE QUERY - END


                $insertChimneyStatus = $connection->query($queryChimneyCaceINSERT);
                if($insertChimneyStatus){
                    $statusMsg = "Kalács sikeresen hozzáadva! ".$errorMsg;
                }
            }else{
                $statusMsg = "Valami hiba lépett fel a hozzáadás során..."; 
            } 
        } 
    }else{ 
        $statusMsg = 'Kérem választjon képet!'; 
    } 
     
    // Display status message 
    echo "<script type='text/javascript'>alert('$statusMsg');</script>";
} 

?>

<div id="content">
<hr>
  <h1>Kalács lista</h1>
<?php
  //$sql = "SELECT * FROM chimney_cakes ORDER BY CHIMNEYCAKE_NAME ASC";

$pathForPicturesDelete = $_SERVER['DOCUMENT_ROOT'].'/pokywebpage/picsChimneycake/';
$pathForPictures = '../../pokywebpage/picsChimneycake/';
//echo $pathForPictures;
//C:\xampp2\htdocs\


  $querySelectChimneyCake = "SELECT 
                            chimney_cakes.id,
                            chimney_cakes.CHIMNEYCAKE_NAME,
                            chimney_cake_images.IMAGE_FILE_NAME,
                            chimney_cakes.CHIMNEYCAKE_DESC,
                            chimney_cakes.CHIMNEYCAKE_FLAVOR,
                            chimney_cakes.CHIMNEYCAKE_KAL,
                            chimney_cakes.CHIMNEYCAKE_ISAVAILABLE,
                            chimney_cakes.CHIMNEYCAKE_IMAGES_ID
                            FROM chimney_cakes 
                            LEFT JOIN chimney_cake_images  
                            ON chimney_cakes.CHIMNEYCAKE_IMAGES_ID = chimney_cake_images.id";

  $result = mysqli_query($connection,$querySelectChimneyCake) or die(mysqli_error($connection));
  if(mysqli_num_rows($result) == 0){
    echo '<p>Jelenleg nincs elérhető kalács hozzáadva!</p>';
  }

  if(mysqli_num_rows($result)>0)
  {
   
    $no=1;
    echo '<table>';
    echo '<tr>
            <th>No.</th>
            <th>Kalács neve</th>
            <th>Kép a kalácsról</th>
            <th>Kalács leírás</th>
            <th>Kalács izesítése</th>
            <th>Kalória</th>
            <th>Elérhető-e</th>
            <th>Műveletek</th>    
        </tr>';
    
    
    while ($record=mysqli_fetch_array($result,MYSQLI_ASSOC))
    {
        if($record['CHIMNEYCAKE_ISAVAILABLE'] == 1){
            $isAvailable = 'igen';
        }
        else{
              $isAvailable = 'nem';
        }

      echo "<tr>";

      echo '<td>'.$no.'.</td>
            <td>'.$record['CHIMNEYCAKE_NAME'].'</td>
            <td><img src="'.$pathForPictures.''.$record['IMAGE_FILE_NAME'].'" width="100" /></td>
            <td>'.$record['CHIMNEYCAKE_DESC'].'</td>
            <td>'.$record['CHIMNEYCAKE_FLAVOR'].'</td>
            <td>'.$record['CHIMNEYCAKE_KAL'].'</td>
            <td>'.$isAvailable.'</td>';
            
            
      echo '<td>
           <a href="deleter.php?page=cake&amp;file='.$record['IMAGE_FILE_NAME'].'&amp;id='.$record['CHIMNEYCAKE_IMAGES_ID'].'">törlés</a>

           </td>';
      echo "</tr>";
      $no++;
    }
    echo '</table>';
    mysqli_free_result($result);
  }

  ?>
</div>