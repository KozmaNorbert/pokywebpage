<?php 
define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
//require("db_config.php");
if(isset($_POST['submit'])){ 
    // Include the database configuration file 
    include_once '../../db_config.php'; 
     
    // File upload configuration 
    $targetDir = "../../../picsChimneycake/"; 
    $allowTypes = array('jpg','png','jpeg','gif'); 
     
    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = ''; 
    $fileNames = array_filter($_FILES['files']['name']); 
    if(!empty($fileNames)){ 
        foreach($_FILES['files']['name'] as $key=>$val){ 
            // File upload path 
            $fileName = basename($_FILES['files']['name'][$key]);
            $targetFilePath = $targetDir . $fileName; 
             
            // Check whether file type is valid 
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
            //echo $fileType;
            if(in_array($fileType, $allowTypes)){ 
                // Upload file to server
                $newFilename = time() . "_" . uniqid() . "." . $fileType;
                if(move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetDir . $newFilename)){ 
                    // Image db insert sql 
                    $insertValuesSQL .= "('".$newFilename."','"."gallery/".$newFilename."','".$newFilename."')"; 
                }else{ 
                    $errorUpload .= $_FILES['files']['name'][$key].' | '; 
                } 
            }else{ 
                $errorUploadType .= $_FILES['files']['name'][$key].' | '; 
            } 
        } 
         
        if(!empty($insertValuesSQL)){ 
            $insertValuesSQL = trim($insertValuesSQL, ','); 
            // Insert image file name into database 
            $insert = $connection->query("INSERT INTO gallery (IMAGE_FILE_NAME, IMAGE_DESC, IMAGE_ALT) VALUES $insertValuesSQL"); 
            if($insert){ 
                $errorUpload = !empty($errorUpload)?'Upload Error: '.trim($errorUpload, ' | '):''; 
                $errorUploadType = !empty($errorUploadType)?'File Type Error: '.trim($errorUploadType, ' | '):''; 
                $errorMsg = !empty($errorUpload)?'<br/>'.$errorUpload.'<br/>'.$errorUploadType:'<br/>'.$errorUploadType; 
                $statusMsg = "Files are uploaded successfully.".$errorMsg; 
            }else{ 
                $statusMsg = "Sorry, there was an error uploading your file."; 
            } 
        } 
    }else{ 
        $statusMsg = 'Please select a file to upload.'; 
    } 
     
    // Display status message 
    
} 
echo "<script type='text/javascript'>alert('$statusMsg');</script>";
?>