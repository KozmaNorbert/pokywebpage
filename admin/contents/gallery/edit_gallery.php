<h1>Galéria szerkeztése</h1><br>

<form action="" method="post" enctype="multipart/form-data">
    Választja ki a képeket feltöltéshez:
    <input type="file" name="files[]" multiple accept="image/*">
    <input type="submit" name="submit" value="Feltöltés">
</form>

<?php 
//define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
//require("db_config.php");
if(isset($_POST['submit'])){ 
    // Include the database configuration file 
    //include_once '../../db_config.php'; 
    
    // File upload configuration 
    $targetDir = "../../../picsGallery/"; 
    $allowTypes = array('jpg','png','jpeg','gif'); 
     
    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = ''; 
    $fileNames = array_filter($_FILES['files']['name']); 
    if(!empty($fileNames)){ 
        foreach($_FILES['files']['name'] as $key=>$val){ 
            // File upload path 
            $fileName = basename($_FILES['files']['name'][$key]);
            echo $fileName;
            $targetFilePath = $targetDir . $fileName; 
             
            // Check whether file type is valid 
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
            //echo $fileType;
            if(in_array($fileType, $allowTypes)){ 
                // Upload file to server
                $newFilename = time() . "_" . uniqid() . "." . $fileType;
                if(move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetDir . $newFilename)){ 
                    // Image db insert sql 
                    $insertValuesSQL .= "('".$newFilename."')"; 
                }else{ 
                    $errorUpload .= $_FILES['files']['name'][$key].' | '; 
                } 
            }else{ 
                $errorUploadType .= $_FILES['files']['name'][$key].' | '; 
            } 
        } 
         
        if(!empty($insertValuesSQL)){ 
            $insertValuesSQL = trim($insertValuesSQL, ','); 
            // Insert image file name into database 
            $insert = $connection->query("INSERT INTO gallery (IMAGE_FILE_NAME) VALUES $insertValuesSQL"); 
            if($insert){ 
                $errorUpload = !empty($errorUpload)?'Upload Error: '.trim($errorUpload, ' | '):''; 
                $errorUploadType = !empty($errorUploadType)?'File Type Error: '.trim($errorUploadType, ' | '):''; 
                $errorMsg = !empty($errorUpload)?'<br/>'.$errorUpload.'<br/>'.$errorUploadType:'<br/>'.$errorUploadType; 
                $statusMsg = "Files are uploaded successfully.".$errorMsg; 
            }else{ 
                $statusMsg = "Sorry, there was an error uploading your file."; 
            } 
        } 
    }else{ 
        $statusMsg = 'Please select a file to upload.'; 
    } 
     
    // Display status message 
    echo "<script type='text/javascript'>alert('$statusMsg');</script>";
} 

?>

<div id="content">
<hr>
  <h1>Galléria lista</h1>
<?php
$pathForPictures = '../../pokywebpage/picsGallery/';


  $querySelectChimneyCake = "SELECT *
                            FROM gallery";

  $result = mysqli_query($connection,$querySelectChimneyCake) or die(mysqli_error($connection));

  if(mysqli_num_rows($result)>0)
  {
   
      
    $no=1;
    echo '<table>';
    echo '<tr>
            <th>No.</th>
            <th>Kép</th>
            <th>Elérhető-e</th>
            <th>Műveletek</th>    
        </tr>';
    
    
    while ($record=mysqli_fetch_array($result,MYSQLI_ASSOC))
    {
        $isAvailable = 'nem';
        if($record['IMAGE_ISAVAILABLE'] == 1){
            $isAvailable = 'igen';
        }

      echo "<tr>";

      echo '<td>'.$no.'.</td>
            <td><img src="'.$pathForPictures.''.$record['IMAGE_FILE_NAME'].'" width="200" /></td>
            <td>'.$isAvailable.'</td>';
            
            
      echo '<td>
      <a href="deleter.php?page=gallery&amp;file='.$record['IMAGE_FILE_NAME'].'&amp;id='.$record['id'].'">törlés</a>

           </td>';
      echo "</tr>";
      $no++;
    }
    echo '</table>';
    mysqli_free_result($result);
  }
  ?>
</div>