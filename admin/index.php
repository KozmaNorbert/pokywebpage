<?php
define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
require("db_config.php");
session_start();

if(! isset($_SESSION['admin']['status']))
{
    header("location:login.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../jquery.datetimepicker.min.css">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->

    <link href="https://fonts.googleapis.com/css?family=Signika+Negative" rel="stylesheet">
    <!--    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"/>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="bootstrap.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="content-language" content="hu"/>
    <meta charset="UTF-8">
    <title>Poky | Admin</title>


</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">PokyAdmin</a>
        </div>
        <a class="nav-item nav-link" href="index.php?page=gallery">Galléria szerkeztése</a>
        <a class="nav-item nav-link" href="index.php?page=cake">Kalács kínálat szerkeztése</a>
        <a class="nav-item nav-link" href="index.php?page=events">Események szerkeztése</a>
        <div>
        <ul class="nav navbar-nav navbar-right">
            <li>Üdv, <?php echo $_SESSION['username'];?></li>
            <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i>Kijelentkezés</a></li>
        </ul>
    </div>
</nav>

</div>


<?php


 $current_page = 'homepage';

 if(array_key_exists("page",$_GET)) {
     $current_page = $_GET["page"];
 }

switch ($current_page) {
    case 'cake':
        include 'contents/cake/edit_cake_supply.php';
        break;
    case 'gallery':
        include 'contents/gallery/edit_gallery.php';
        break;
    case 'events':
            include 'contents/events/add_event.php';
            break;   
    case 'homepage':
    default:
        include 'contents/home/homepage.php';
}
?>


<script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZh0sEEand0YGBf13FyQuiTwlAQgxVSNgt4="></script>
<script src="../jquery.js"></script>
<script src="adminJavaScript.js"></script>
<script src="../jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
 
 
 //call ajax
/*    var ajax = new XMLHttpRequest();
    var method = "GET";
    var url = ".php";
    var asynchronous = true;

    ajax.open(method, url, asynchronous);
    //sending data
    ajax.send();

    //reciving response from data.php
    ajax.onreadystatechange = function () {
        if((this.readyState == 4)  &&  (this.status == 200))
        {
            //converting data
           var data = JSON.parse(this.responseText);
           console.log(data);
           //document.getElementById("str").innerHTML=this.responseText;
            var html ="";
            var len = data.length;
           for(var i = 0;len > i;i++)
           {
               var c_date = data[i].c_date;
               var c_desc = data[i].c_desc;
                html += c_date + c_desc;
           }
           //document.getElementById("str").innerHTML = html;
        }
    }*/



    /////datePicker
 $("#datetime").datetimepicker();


    ///real time search
    $('#search_text').keyup(function () {

        var txt = $(this).val();
        if(txt == '')
        {
            $('#result_search').html("");
        }
        else
        {
            $('#result').html('');
            $.ajax({
               url:"search.php",
               method:"post",
               data:{search:txt},
               dataType:"text",
               success:function (data)
               {
                    $('#result_search').html(data);

               }
            });
        }
return true;
    });



 function listConcert(id){

     var c_date = document.getElementById("datetime").value;
     var c_event = document.getElementById("event").value;
     var c_place = document.getElementById("place").value;
     var c_description = document.getElementById("description").value;
     var c_facebook = document.getElementById("face").value;
     var c_official = document.getElementById("official").value;
     document.getElementById("")


     //alert();
     var id2 = id;
     $.ajax({
         url: "data.php",
         method:"post",
         data: {c_id: id2},
         dataType: "text",
         success: function (data) {

             json = JSON.parse(data);
             console.log(data);

             var len = Object.keys(json).length;
             var len2 = json.length;
             console.log(len2);
             for(var i = 0;len2 > i;i++)
             {
                
                 c_date= json[i].c_date;
                 c_event=json[i].c_event;
                 c_place=json[i].c_place;
                 c_description=json[i].c_desc;
                 c_facebook=json[i].c_facebook;
                 c_official = json[i].c_official;
                 console.log(c_date);
             }

           
             document.getElementById("datetime").value = c_date;
             document.getElementById("event").value = c_event;
             document.getElementById("place").value = c_place;
             document.getElementById("face").value = c_facebook;
             document.getElementById("description").value = c_description;
             document.getElementById("official").value = c_official;


         }
     });

return true;


 }


 function deleteConcert(id){
     var id2=id;
     if(confirm("Biztosan törlni szeretnéd?"))
     {
         $.ajax({
         url: "delete_concert.php",
         method:"post",
         data: {c_id: id2},
         dataType: "text",
         success: function (data) {
             alert(data);
             location.reload();
         }
     });
     //alert("deleted");
     }
     else
         {
     }
     return true;
 }



 /*adding form*/
 function saveConcert() {



     var c_date = document.getElementById("datetime").value;
     var c_event = document.getElementById("event").value;
     var c_place = document.getElementById("place").value;
     var c_description = document.getElementById("description").value;
     var c_facebook = document.getElementById("face").value;
     var c_official = document.getElementById("official").value;

     $.ajax({
         url: "add_new.php",
         method:"post",
         data: {
             c_date: c_date,
             c_event: c_event,
             c_place: c_place,
             c_desc: c_description,
             c_facebook: c_facebook,
             c_official: c_official
         },
         dataType: "text",
         success: function (data) {
             alert(data + ': nevű fellépés sikeresen hozzáadva!');
         }
     });
     alert("saved");

 }





    
</script>


</body>
</html>

