<!doctype html>
<html lang="en">

<head>
  <title>Poky | Események</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/mainPage.css">

</head>

<body>
<a href="index.php">
<img alt="hun" src="images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">
<img alt="srb" src="images/srb.png" width="30" height="20">
</a>
  <div class="top-container">
    <div><img src="images/1.jpg" width="100%" alt="" class="top-container"></div>
    <div><img src="images/2.jpg" width="100%" alt="" class="top-container"></div>
    <div><img src="images/3.jpg" width="100%" alt="" class="top-container"></div>
  </div>
  <!--Navbar-->

  <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
    <img src="images/logo.png" width="4%" alt="">
    <div class="nav navbar-nav">

      <a class="nav-item nav-link" href="index.php">Főoldal</a>
      <a class="nav-item nav-link" href="about.php">Rólunk</a>
      <a class="nav-item nav-link" href="history.php">Kürtőskalácsról</a>
      <a class="nav-item nav-link" href="supply.php">Kínálat</a>
      <a class="nav-link active" href="events.php">Események</a>
      <a class="nav-item nav-link" href="gallery.php">Galéria</a>
      <a class="nav-item nav-link" href="search.php">Keresés</a>
      
    </div>
  </nav>
  <br>

 
  <?php
      
      define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
      require("admin/db_config.php");

      $querySelectEvents = "SELECT id,
      name_hun,
      name_srb,
      start_date,
      end_date,
      place_hun,
      place_srb
      from events group by DATE(start_date) asc;
    ";

  $result = mysqli_query($connection,$querySelectEvents) or die(mysqli_error($connection));
 $no = 1;
  if(mysqli_num_rows($result)>0)
    {
    echo'<table class="table">
    <thead class="thead-light">
      <tr>
      <th scope="col">#</th>
        <th scope="col">Időpont</th>
        <th scope="col">Rendezvény neve</th>
        <th scope="col">Rendezvény helyszíne</th>
      </tr>
    </thead>
    <tbody>';


      while ($record=mysqli_fetch_array($result,MYSQLI_ASSOC))
      {
         echo'<tr>
         <th scope="row">'. $no.'</th>
         <td>'.$record['start_date'].'-tol '.$record['end_date'].'-ig</td>
         <td>'.$record['name_hun'].'</td>
         <td>'.$record['place_hun'].'</td>
       </tr>';
       $no++;
      }

      echo'   
      </tbody>
    </table>
        
          <br><br>
        
      <br>
      <br>';
    }
  ?>
 
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Felíratkozás</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail cím">
          <button type="button" class="btn btn-light">Küldés</button><br>
          <p><a href="index.php">Tovabbi informació</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>