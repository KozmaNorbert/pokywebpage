<!doctype html>
<html lang="en">

<head>
  <title>Poky | Rólunk</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/mainPage.css">

</head>
</head>

<body>
  <?php
  define("SECRET", "@3eweHjdsdfuihjhjhj#VGVgggg!");
  require("admin/db_config.php");
  ?>
     <a href="index.php">
<img alt="hun" src="images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">
<img alt="srb" src="images/srb.png" width="30" height="20">
</a>
  <div class="top-container">
    <div><img src="images/1.jpg" width="100%" alt="" class="top-container"></div>
    <div><img src="images/2.jpg" width="100%" alt="" class="top-container"></div>
    <div><img src="images/3.jpg" width="100%" alt="" class="top-container"></div>
  </div>
  <!--Navbar-->

  <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
    <img src="images/logo.png" width="4%" alt="">
    <div class="nav navbar-nav">

      <a class="nav-item nav-link" href="index.php">Főoldal</a>
      <a class="nav-link active" href="about.php">Rólunk</a>
      <a class="nav-item nav-link" href="history.php">Kürtőskalácsról</a>
      <a class="nav-item nav-link" href="supply.php">Kínálat</a>
      <a class="nav-item nav-link" href="events.php">Események</a>
      <a class="nav-item nav-link" href="gallery.php">Galéria</a>
      <a class="nav-item nav-link" href="search.php">Keresés</a>
    </div>
  </nav>
  <br>
  

  <div class="test-content">
    <div class="test text">
      <h2>Családi vállalkozásunk rövid története</h2><br>
      
      Családunk 2007-ben kezdett el kürtőskalács készítésével foglalkozni Szabadkán és a környező településeken. 
      Édesapám lakatos így minden szükséges eszközt, gépet, asztalt és az első sátrunkat is ő készítette.A két kezével szabta és 
      vágta ki, majd hegesztette össze a kürtőskalácsot forgató gép részeit, illetve ő találta ki, hogyan működjön az egész szerkezet.
       Az ő fejéből pattant ki az is, hogy a motornak milyen gyorsan kell forognia, hogy az megfelelő legyen.  Több év tapasztalatából 
       nyertük el a mai tészta állagát, formáját és ízét. Valamiért láttunk benne egy üzleti potenciált, és miután elkészült az első
        sátor, az első gép, valamint a tésztagyúráshoz és az árusításhoz szükséges eszközök, akkor belevágtunk. Kezdetben vásárokra és
         különböző piacokra jártunk értékesíteni Szerbiában újdonságnak számító ízletes kalácsunkat. Az emberek figyelmét felkeltette ez az újdonság , hogy mi is az ami forog a gépben és parázson sül. Kezdetben sokat kellet mesélni a kalácsról és annak eredetéről, hogy az emberek megismerjék és egy életre megszeressék ezt a sokak számára különleges kalácsot. Manapság már szinte nincs is olyan település Vajdaságban, ahol ne hallottak volna róla.
      Fokozatosan építettük ki piacunkat egyre több rendezvényre kaptunk meghívást Vajdaság szerte, majd pedig Szerbia szerte. 
      Ezáltal szinte az egész országban ismerté vált kalácsunk és cégünk.Székhelyünk Szabadkán található, de a kalácsok készítése és értekesítése többnyire rendezvényeken történik.
      Kezdetben 6 ízben kínáltuk kalácsainkat (fahéj, mogyoró, kakaó, dió, kókusz, vanília), majd próbálkoztunk új ízek bevezetésével
       is ilyen például a nutellás kekszes, nutellás kókuszos, nutellás mogyorós, majd pár évre rá a zserbós, melyeket vásárlóink 
       előszeretettel fogyasztanak.
      Termékeink bármikor megrendelhetőek, bármilyen eseményre, házibulira, születésnapra, babazsúrra, lakodalomra, eljegyzésre k
      ilátogatunk, és friss, meleg kalácsot szolgálunk fel.
      Minden hónap vasárnapjain vásárokba járunk első vasárnap Zentára, második vasárnap Topolyára, harmadik vasárnap pedig 
      Csantavérre.
      Nyáron koncerteken sütünk, télen pedig részt veszünk a 40 napos karácsonyi vásárban Szabadkán.<br>
      <br>
      Munkamenet<br>
      <br>
      Azon túl, hogy a tésztakészítők a titkos összetevőkből elővarázsolják a finom tésztát,feltekerjük a sütődorongra az édes
       kelt tésztából sodort tésztaszalagot, a csíkok egymásra tapadnak, majd egybesülnek. Ezután a gőzölgő tésztát megforgatjuk
        kakaóban, cukorban, dióban, kókuszreszelékben vagy bármilyen más finomságban, a gép pedig aranybarnára süti, a csapat tagjait
         előkészületek és utómunkálatok várják egy-egy fesztivál során.
      Vannak egy- és többnapos rendezvények, ehhez alkalmazkodnak az objektumok is. Előfordul, hogy faházban dolgozunk, máshol viszont
       csupán szétszedhető asztalokra és sátorra van szükségünk. A kora reggeli órákban elindulunk a vásárba, és miután összeállítottuk
        a sátrat, elkezdjük a helyszínen meggyúrni és megsütni a tésztát. A vásár után körülbelül félórányi munkánk van, ám ez is attól
         függ, hogy házikóban vagy sátorban árulunk-e. Eloltjuk a tüzet, elmosogatunk, és mindent elrakunk másnapra.





      <br><br>
    </div>
  </div>
  <br>
  <br>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Felíratkozás</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail cím">
          <button type="button" class="btn btn-light">Küldés</button><br>
          <p><a href="index.php">Tovabbi informació</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>