<!doctype html>
<html lang="en">

<head>
  <title>Poky | Kürtöskalácsról</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/mainPage.css">

</head>
</head>

<body>
  <?php
  define("SECRET", "@3eweHjdsdfuihjhjhj#VGVgggg!");
  require("admin/db_config.php");
  ?>
      <a href="index.php">
<img alt="hun" src="images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">
<img alt="srb" src="images/srb.png" width="30" height="20">
</a>
  <div class="top-container">
    <div><img src="images/1.jpg" width="100%" alt="" class="top-container"></div>
    <div><img src="images/2.jpg" width="100%" alt="" class="top-container"></div>
    <div><img src="images/3.jpg" width="100%" alt="" class="top-container"></div>
  </div>
  <!--Navbar-->

  <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
    <img src="images/logo.png" width="4%" alt="">
    <div class="nav navbar-nav">

      <a class="nav-item nav-link" href="index.php">Főoldal</a>
      <a class="nav-item nav-link" href="about.php">Rólunk</a>
      <a class="nav-link active" href="history.php">Kürtőskalácsról</a>
      <a class="nav-item nav-link" href="supply.php">Kínálat</a>
      <a class="nav-item nav-link" href="events.php">Események</a>
      <a class="nav-item nav-link" href="gallery.php">Galéria</a>
      <a class="nav-item nav-link" href="search.php">Keresés</a>
    </div>
  </nav>
  <br>
  

  <div class="test-content">
    <div class="test text">
      <h2>A kürtőskalács története</h2><br>
      
      A kürtőskaláccsal kapcsolatban máig is sok vita kering, hogy valójában kürtös kalácsról vagy kürtőskalácsról van-e szó, ha a
       fenti süteményt emlegetik.
       Némelyek úgy tartják, hogy a kürtös kalács elnevezés a találóbb, mert ez a sütemény a honfoglaláskori időkig vezethető vissza, 
       ugyanis a portyázó magyarok, ha lisztet és tojást zsákmányoltak, tésztát gyúrtak belőle, majd ezt kürtjeikre csavarva megsütötték 
       a tábortűznél. Mások pedig úgy vélik, hogy a kürtőskalácsnak semmi köze a honfoglaló magyarokhoz, mivel ezt a süteményt csak akkor
        készítették, amikor már megtelepedtek és stabil konyhával rendelkeztek.</br></br>
       <img src="allPictures/1.jpg" alt="Image" class="img-fluid text test-content"></br></br>

       Egy másik elmélet szerint a kürtőskalács a kályhacsőről kapta a nevét, hiszen Székelyföldön a kályhacsövet kürtőcsőnek nevezik,
        a kürtőskalács sütőfa nagysága és átmérője is erre utal. A leleményes székely a keményfa parazsát, amit a főzéshez és 
        melegedéshez használt, még valamire fel akarta használni, ezért az izzó parazsat kivették a kályha tetejére, vagy a kemence 
        elé, és ott a parázs fölött forgatva sütötték meg e finomságot.</br></br>

        A kürtőskalács eredetéről több hiedelem és monda járja a világot. Természetesen mi azt valljuk magunkénak, amelyik hozzánk a
         legközelebb áll. Ez a tatár-járáshoz kötődik: "A tatár seregek közeledtére a kezdeti ellenállások után, Székelyföld lakossága 
         jobbnak látta elmenekülni.Voltak akik a hegyekbe menekültek, mások pedig a Budvár és a Rez barlangjaiban leltek menedéket. A 
         tatárok pedig, hogy sem megtámadni, sem kicsalogatni nem tudták a biztos és megközelíthetetlen búvóhelyen levőket,
          elhatározták, hogy kiéheztetik a székelyeket. Ez így ment hosszú ideig, amíg egyszer csak a tatároknak és a székelyeknek is 
          elfogyott az ennivalójuk. Ekkor egy okos székely asszony összekaparta a maradék lisztet, hamuval összekeverte, és hatalmas 
          kalácsokat sütött, amelyeket dorongra vagy magas póznákra húztak és felmutatták a tatároknak: "Nézzétek mi itt milyen jól 
          élünk, míg ti pedig éheztek." A tatárok akik, már alig bírták az éhezést, bosszúsan elvonultak."
          </br></br>
          <img src="allPictures/430.jpg" alt="Image" class="img-fluid text test-content"></br></br>

      <br><br>
    </div>
  </div>
  <br>
  <br>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Felíratkozás</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail cím">
          <button type="button" class="btn btn-light">Küldés</button><br>
          <p><a href="index.php">Tovabbi informació</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>