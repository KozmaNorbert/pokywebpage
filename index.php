<!doctype html>
<html lang="en">
  <head>
    <title>Poky | Főoldal</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/mainPage.css">
    
  </head>
</head>
  <body>
    <?php
        define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
        require("admin/db_config.php");
    ?>
     <a href="index.php">
<img alt="hun" src="images/hun.png" width="30" height="20">
</a>

<a href="srb/index.php">
<img alt="srb" src="images/srb.png" width="30" height="20">
</a>
      <div class="top-container">
        <div><img src="images/1.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="images/2.jpg" width="100%"  alt="" class="top-container"></div>
        <div><img src="images/3.jpg" width="100%"  alt="" class="top-container"></div> 
      </div>
    <!--Navbar-->
   
      <nav class="navbar navbar-expand font-weight-normal bg-light justify-content-center">
        <img src="images/logo.png" width="4%"  alt="" >
          <div class="nav navbar-nav">
              
              <a class="nav-link active" href="index.php">Főoldal</a>
              <a class="nav-item nav-link" href="about.php">Rólunk</a>
              <a class="nav-item nav-link" href="history.php">Kürtőskalácsról</a>
              <a class="nav-item nav-link" href="supply.php">Kínálat</a>
              <a class="nav-item nav-link" href="events.php">Események</a>
              <a class="nav-item nav-link" href="gallery.php">Galéria</a>
              <a class="nav-item nav-link" href="search.php">Keresés</a>
          </div>
      </nav>    
      <br>
      <br>
      <br>
      <div class="test-content">
        <div class="test text" >
        Kedves Érdeklődő!<br>
        <br>
        A “POKY Kürtőskalács” már 11 éve áll a kedves vásárlók rendelkezésére. Felléptünk már nagyon sok gasztronómiai kiállításon, vásárokon, különböző rendezvényeken, mint például a palicsi május elseje, szintén palicsi sonkanapok, arató ünnepségek, magyarkanizsai illetve palicsi Szent István nap, Újvidéken a kenyérfesztivál, stb..
        <br>
        Sajátosságaink:<br>
      -  friss termékek, hiszen kézzel és helyben dagasztjuk a tésztát,<br>
      - a különböző ízvariációk választéka,<br>
      - a vásárló a kürtőskalács készítés összes műveletét végig követheti,<br>
      - alkalmazottaink egyedi tervezésű, “POKY” logóval ellátott munkaruhát viselnek.<br>
      <br>
     
      Hagyományos, helyben készített kürtőskalács előállítása, hagyományőrző vásárokon, fesztiválokon, mesterség bemutatókon, gasztronómiai kiállításon, különböző összejöveteleken, bálokon, vásáron vagy bármilyen jellegű szervezett rendezvényen.
      <br>
      
      <br>
      Kedves Vásárlóink,

Tekintettel arra, hogy a közeljövőben nem lesznek rendezvények, ezért úgy döntöttünk házhoz visszük a kürtőskalácsot 


    </div>
    <div class="main-page-image" >
        <img src="allPictures/cimetke.jpg" alt="Image" class="img-fluid text">
        </div>
    </div>
      </div>
     

    <section class="bg-info text-center p-5 mt-4">
      <div class="container p-3">
              <br>
              <br>
              <h3>Kedves Látogató!</h3><br>
  <p>Ha nem szeretne lemaradni a rendezvényekről, melyeken mi is részt veszünk, akkor íratkozzon fel az oldalunkra! Nem kell mást tennie, mint megadni az email címét, és mi értesítjük a közelgő eseményekről!<p>
        
       
        <form action="" method="post">
          <input type="text" name="email_address" placeholder="Adja meg az e-mail címét  ">
          <input type="submit" name="submit" value="Felíratkozás" class="btn btn-light">
        </form>

        <?php
        if(isset($_POST['submit'])){ 
        
          $email = $_POST["email_address"];
        $emailErr = "Helytelen email forma!";
        $emailSuccess = "Felíratkozás elfogadva";
        $emailInsertError = "Valami hiba történt, kérem próbálja újra!";
        $emailIsExist = "Ezel az email-el mar fel iratkozott";

        $querySelectEmail = "SELECT * FROM subscribers WHERE EMAIL = '$email'";
        $result = mysqli_query($connection,$querySelectEmail) or die(mysqli_error($connection));
        if(mysqli_num_rows($result) == 0){
         
      

          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo "<script type='text/javascript'>alert('$emailErr');</script>";
          }
          else{

              $queryUploadPicture = "INSERT INTO subscribers (EMAIL) VALUES ('$email')"; 
              $insert = $connection->query($queryUploadPicture);
                
                if ($insert) {
                  echo "<script type='text/javascript'>alert('$emailSuccess');</script>";
                }
                else {
                  echo "<script type='text/javascript'>alert('$emailInsertError');</script>";
                }
        }
        }
     
      elseif(mysqli_num_rows($result) != 0){
        echo "<script type='text/javascript'>alert('$emailIsExist');</script>";
      }
    }
        ?>
        
        <br><br>
      </div>
    </section>
   <br>
   <br>
     
     
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
    <footer class="bg-light" >
      <div class="test-content footer">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <br>
          <h6 class="text-uppercase font-weight-bold">Poky Kolač Subotica PR</h6>
      
          <i class="fa fa-phone mr-3"></i> 060/042-11-90<br>    <br>
            <i class="fa fa-phone mr-3"></i> 062/129-90-29<br> <br>
        </div>
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" >
          <br>
          <h6 class="text-uppercase font-weight-bold">Felíratkozás</h6><br>
          <form action="#" method="Post">
          <input type="text" name="text" placeholder="E-mail cím">
          <button type="button" class="btn btn-light">Küldés</button><br>
          <p><a href="index.php">Tovabbi informació</a></p>
              </form>
              <a href="https://www.facebook.com/pokykolac/"><img src="images/facebook.png" alt="Image" width="20%"></a>
              <a href="https://www.instagram.com/poky.kurtoskalacs/"><img src="images/instagram.png" alt="Image" width="20%"></a>
      </div>
    </div>
    </footer>
  </body>
</html>